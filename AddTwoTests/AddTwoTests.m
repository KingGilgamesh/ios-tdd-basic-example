//
//  AddingTwoNumbersTests.m
//  AddingTwoNumbersTests
//
//  Created by Mate Panyor on 06/05/15.
//  Copyright (c) 2015 Mate Panyor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RSAddition.h"

@interface AddingTwoNumbersTests : XCTestCase

@end

@implementation AddingTwoNumbersTests {
    RSAddition *addition;
}

- (void)setUp {
    [super setUp];
    addition = [[RSAddition alloc]init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    //XCTAssert(YES, @"Pass");
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testAdditionClassExists {
    XCTAssertNotNil(addition,@"RSAddition class exists");
}

- (void)testAddTwoPlusTwo {
    NSInteger result = [addition addNumberOne:2 withNumberTwo:2];
    XCTAssertEqual(result, 4, @"Addition of 2 + 2 is 4");
}

- (void)testAddTwoPlusSeven {
    NSInteger result = [addition addNumberOne:2 withNumberTwo:7];
    XCTAssertEqual(result, 9, @"Addition of 2 + 7 is 9");
}

@end
