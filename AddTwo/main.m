//
//  main.m
//  AddTwo
//
//  Created by Mate Panyor on 06/05/15.
//  Copyright (c) 2015 Mate Panyor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
