//
//  AddTwoNumberViewControllerTests.m
//  AddTwo
//
//  Created by Mate Panyor on 14/05/15.
//  Copyright (c) 2015 Mate Panyor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RSViewController.h"

@interface AddTwoNumberViewControllerTests : XCTestCase

@end

@implementation AddTwoNumberViewControllerTests
{
    RSViewController* viewController;
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    viewController = [storyboard instantiateViewControllerWithIdentifier:@"RSViewController"];
    [viewController view];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    //XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)testViewControllerViewExists {
    XCTAssertNotNil([viewController view], @"ViewController should contain a view");
}

- (void) testFirstNumberTextFieldConnection {
    XCTAssertNotNil([viewController firstNumberTextField],@"firstNumberTextField should be connected");
}

- (void) testSecondNumberTextFieldConnection {
    XCTAssertNotNil([viewController secondNumberTextField], @"secondNumberTextField should be connected");
}
- (void) testresultTextFieldConnection {
    XCTAssertNotNil([viewController resultLabel], @"resultTextField should be connected");
}

- (void) testAddButtonConnection {
    XCTAssertNotNil([viewController addButton],@"add button should be connected");
}
@end
