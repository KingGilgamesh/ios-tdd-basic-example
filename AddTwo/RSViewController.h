//
//  RSViewController.h
//  AddTwo
//
//  Created by Mate Panyor on 14/05/15.
//  Copyright (c) 2015 Mate Panyor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSViewController : UIViewController

@property (nonatomic,strong) IBOutlet UITextField* firstNumberTextField;
@property (nonatomic,strong) IBOutlet UITextField* secondNumberTextField;
@property (nonatomic,strong) IBOutlet UILabel* resultLabel;
@property (nonatomic,strong) IBOutlet UIButton* addButton;


@end
