//
//  RSAddition.m
//  AddingTwoNumbers
//
//  Created by Mate Panyor on 06/05/15.
//  Copyright (c) 2015 Mate Panyor. All rights reserved.
//

#import "RSAddition.h"

@implementation RSAddition

-(NSInteger)addNumberOne:(NSInteger) firstNumber withNumberTwo:(NSInteger) secondNumber {
    return firstNumber+secondNumber;
}

@end


