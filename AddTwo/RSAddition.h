//
//  RSAddition.h
//  AddingTwoNumbers
//
//  Created by Mate Panyor on 06/05/15.
//  Copyright (c) 2015 Mate Panyor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSAddition : NSObject

- (NSInteger)addNumberOne:(NSInteger) firstNumber withNumberTwo:(NSInteger) secondNumber;

@end
